import { DELETE_USER, FETCH_USER, FETCH_USERS, SEARCH_USERS, CREATE_USER, EDIT_USER } from '../actions/UserActions'
import update from 'immutability-helper'

const initialState = {
  users: [],
  ids: [],
  search: { username: undefined },
  isFetching: false,
  isError: false,
  error: null
}

function users(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USERS:
      if (isOk(action)) {
        return update(state, {
          search: { [action.payload.field]: { $set: action.payload.value } }
        })
      }
      return state
    case FETCH_USER:
      if (isPending(action)) {
        let newState = Object.assign({}, state, { isFetching: true, isError: false, error: null })
        if (state.ids.indexOf(action.meta.id) === -1) {
          newState.ids.push(action.meta.id)
        }
        newState.users[action.meta.id] = {}
        return newState
      }
      if (isOk(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: false, error: null })

        newState.users[action.meta.id] = action.payload
        return newState
      }
      if (isError(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: true, error: action.payload })
        return newState
      }
      return state
    case CREATE_USER:
      console.log(action)
      if (isPending(action)) {
        let newState = Object.assign({}, state, { isFetching: true, isError: false, error: null })

        return newState
      }
      if (isOk(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: false, error: null })
        newState.ids.push(action.payload.id)
        newState.users[action.payload.id] = action.payload
        return newState
      }
      if (isError(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: true, error: action.payload })
        return newState
      }
      return state

    case EDIT_USER:
      if (isPending(action)) {
        let newState = Object.assign({}, state, { isFetching: true, isError: false, error: null })
        if (state.ids.indexOf(action.meta.id) === -1) {
          newState.ids.push(action.meta.id)
        }
        newState.users[action.meta.id] = {}
        return newState
      }
      if (isOk(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: false, error: null })

        newState.users[action.payload.id] = action.payload
        return newState
      }
      if (isError(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: true, error: action.payload })
        return newState
      }
      return state

    case FETCH_USERS:
      if (isPending(action)) {
        let newState = Object.assign({}, state, { isFetching: true, isError: false, error: null })
        return newState
      }
      if (isOk(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: false, error: null, ids: [], users: [] })
        action.payload.forEach((user) => {
          newState.ids.push(user.id)
          newState.users[user.id] = user
        })

        return newState
      }
      if (isError(action)) {
        let newState = Object.assign({}, state, {
          isFetching: false,
          isError: true,
          error: action.payload,
          ids: [],
          users: []
        })
        return newState
      }
      return state

    case DELETE_USER:
      console.log(action)
      if (isPending(action)) {
        let newState = Object.assign({}, state, { isFetching: true, isError: false, error: null })
        if (state.ids.indexOf(action.meta.id) === -1) {
          newState.ids.push(action.meta.id)
        }
        newState.users[action.meta.id] = {}
        return newState
      }
      if (isOk(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: false, error: null })
        update(newState, { users: { $splice: [[action.meta.id, 1]] }, ids: { $splice: [[action.meta.id, 1]] } })
        return newState
      }
      if (isError(action)) {
        let newState = Object.assign({}, state, { isFetching: false, isError: true, error: action.payload })
        return newState
      }
      return state

    default:
      return state
  }
}

export default users

export const getAll = (state) => state.ids.map((id) => state.users[id])

export const getById = (state, userId) => (state.users[userId])

export function isPending(action) {
  return !action.hasOwnProperty('error')
}

export function isOk(action) {
  return action.error === false
}

export function isError(action) {
  return action.error === true
}

export function isBlocking(action) {
  return action.meta && action.meta.isBlocking === true
}