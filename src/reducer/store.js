import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import users from './users'

export const configureStore = () => {
  const store = createStore(
    combineReducers({
      users,
      form: formReducer
    }),
    composeWithDevTools(
      applyMiddleware(
        thunk,
        logger
      )
    )
  );

  return store;
}
