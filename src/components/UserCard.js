import React, { Component } from 'react'
import {
    Card, CardBody, CardTitle, CardSubtitle, ListGroupItem,
    ListGroupItemHeading, ListGroupItemText, Row, Button
} from 'reactstrap'
import Address from './Address'
import Company from './Company'

class UserCard extends Component {
    render = () => {
        const user = this.props.user

        return user && <Card onClick={() => this.props.onClick(user)}>
            <CardBody>
                <Row className="d-flex justify-content-between col-12 m-1"><div><CardTitle>{user.name}</CardTitle>
                    <CardSubtitle>{user.username}</CardSubtitle></div>
                    {!this.props.short &&
                        <Row>
                            <Button onClick={() => this.props.onEdit(user.id)}>Edit</Button>
                            <Button onClick={() => this.props.onDelete(user.id)}>Delete</Button>
                        </Row>}
                </Row>

                {!this.props.short && <div className="container">
                    <div className="row">
                        <ListGroupItem className="col-lg-4 col-md-6">
                            <ListGroupItemHeading>Email</ListGroupItemHeading>
                            <ListGroupItemText>{user.email}</ListGroupItemText>
                        </ListGroupItem>
                        <ListGroupItem className="col-lg-4 col-md-6">
                            <ListGroupItemHeading>Phone</ListGroupItemHeading>
                            <ListGroupItemText>{user.phone}</ListGroupItemText>
                        </ListGroupItem>

                        <ListGroupItem className="col-lg-4 col-md-6">
                            <ListGroupItemHeading>Website</ListGroupItemHeading>
                            <ListGroupItemText>{user.website}</ListGroupItemText>
                        </ListGroupItem>

                        <ListGroupItem className="col-lg-6 col-md-6">
                            <ListGroupItemHeading>Address</ListGroupItemHeading>
                            <Address address={user.address} />

                        </ListGroupItem>
                        <ListGroupItem className="col-lg-6 col-md-6">
                            <ListGroupItemHeading>Company</ListGroupItemHeading>
                            <Company company={user.company} />
                        </ListGroupItem>
                    </div>
                </div>
                }
            </CardBody>
        </Card>
    }
}

export default UserCard