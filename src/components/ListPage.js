import React, { Component } from 'react'
import UserCard from './UserCard'
import FiltersForm from './FiltersForm'
import UserForm from './UserForm'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { fetchUsers, fetchUser, editUser, deleteUser, addUser, filterUsers } from '../actions/UserActions'
import { SubmissionError } from 'redux-form'
import { Loading } from './Loading'
import { getAll } from '../reducer/users'
import { Button } from 'reactstrap';

class ListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            edited: null,
            created: false,
            isLoading: false
        }
    }

    onFilter = (data) => {
        const { actions: { filterUsers } } = this.props
        if (data) {
            const { username } = data
            filterUsers('username', username)
        } else {
            filterUsers('username', '')
        }
    }

    onCancelEdit = (user) => {
        this.setState({ edited: null})
    }

    onCancelCreate = () => {
        this.setState({ created: false })
    }

    onSelectUser = (user) => {
        if (!this.state.selected || user.id !== this.state.selected) {
            this.setState({ selected: user.id })
        } else {
            this.setState({ selected: null })
        }
    }

    onSelectUserEdit = (userId) => {
        this.setState({ selected: null, edited: userId, created: false })
    }

    renderDetail = (userId) => {
        const user = this.props.users.find((user) => (user.id === userId))

        if (!user) {
            return <div></div>
        }
        return (
            <div className="col-12 col-lg-12 col-md-12">
                <UserCard user={user} onClick={() => this.onSelectUser(user.id)} onEdit={this.onSelectUserEdit} onDelete={this.onDeleteUser} />
            </div>
        )
    }

    renderEdit = (userId) => {
        console.log('renderEdit',userId)
        if (!userId) {
            return <div></div>
        }
        const user = this.props.users.find((user) => (user.id === userId))
        console.log('renderEdit',userId)
        if (!user) {
            return <div></div>
        }
        return (
            <div className="col-12 col-lg-12 col-md-12">
                <UserForm onSubmit={this.onEditUser} onCancel={this.onCancelEdit} initialValues={user} />
            </div>
        )
    }

    renderCreate = () => {
        const user = {
            "id": "",
            "name": "",
            "username": "",
            "email": "",
            "address": {
                "street": "",
                "suite": "",
                "city": "",
                "zipcode": "",
                "geo": { "lat": "", "lng": "" }
            },
            "phone": "",
            "website": "",
            "company": { "name": "", "catchPhrase": "", "bs": "" }
        }
        return (
            <div className="col-12 col-lg-9 col-md-10 m-1" >
                <UserForm onSubmit={this.onCreateUser} onCancel={this.onCancelCreate} initialValues={user} />
            </div>
        )
    }

    onDeleteUser = (id) => {
        this.props.actions.deleteUser(id).then(() => {

        })
    }
    onEditUser = (user) => {
        const { actions: { editUser } } = this.props
        const id = this.state.edited
        this.setState({ edited: null })
        return editUser(id, user)
            .catch((error) => {
                throw new SubmissionError(error)
            })
    }

    onCreateUser = (user) => {
        const { actions: { addUser } } = this.props
        this.setState({ created: false })
        return addUser(user)
            .catch((error) => {
                throw new SubmissionError(error)
            })
    }

    fetchUsers = () => {
        const { actions: { fetchUsers }, search } = this.props

        if (!this.state.isLoading) {
            this.setState({ isLoading: true })
            fetchUsers(search)
                .then((res) => {
                    this.setState({ isLoading: false })
                    return res;
                })
                .catch((err) => {
                    this.setState({ isLoading: false })
                    return Promise.reject(err)
                })
        }
    }

    componentDidMount() {
        return this.fetchUsers()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.search !== this.props.search) {
            this.fetchUsers()
        }
    }

    render = () => {
        const { error, users, search } = this.props

        const items = users.length ? users.filter((user) => (user.id && user.id !== this.state.selected))
            .map((user) => {
                return (
                    <div key={user.id} className="col-12 col-lg-3 col-md-6">
                        <UserCard user={user}
                            short={true}
                            onClick={() => this.onSelectUser(user)} />
                    </div>
                )
            }) : <div></div>

        if (!!error) {
            return (<div className="container"><div className="row">{this.props.error}}</div></div>)
        }
console.log(this.state)
        return (
            <div className="container">
                <div className="row">
                    <FiltersForm onSubmit={(values) => this.onFilter(values)} initialValues={search} />
                </div>
                <div className="row">
                    <Button onClick={() => this.setState({ created: true, selected: null, edited: null })} disabled={!!this.state.created || !!this.props.isFetching}>Add User</Button>
                </div>
                <div className="row">
                    {!!this.state.selected ? this.renderDetail(this.state.selected) : null}
                    {!!this.state.edited ? this.renderEdit(this.state.edited) : null}
                    {!!this.state.created ? this.renderCreate() : null}
                </div>
                <div className="row">
                    {!!this.props.isFetching ? <div className="container"><div className="row"><Loading /></div></div> : items}
                </div>
            </div>

        )
    }

}

const mapStateToProps = (state, props) => {
    const users = state.users ? getAll(state.users) : []

    return {
        users,
        search: state.users.search,
        isFetching: !!users.isFetching,
        isError: !!users.isError,
        error: users.error
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ fetchUsers, fetchUser, editUser, deleteUser, addUser, filterUsers }, dispatch)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps)
    (ListPage)
