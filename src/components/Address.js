import React, { Component } from 'react'
import { ListGroupItemHeading, ListGroupItemText } from 'reactstrap'

class Address extends Component {
    render = () => {
        const address = this.props.address

        return address &&
            !this.props.short && <div className="container">
                <div className="row">
                    <div className="col-12">
                        <ListGroupItemHeading>Street</ListGroupItemHeading>
                        <ListGroupItemText>{address.street}</ListGroupItemText>
                    </div>
                    <div className="col-12">
                        <ListGroupItemHeading>Suite</ListGroupItemHeading>
                        <ListGroupItemText>{address.suite}</ListGroupItemText>
                    </div>

                    <div className="col-12">
                        <ListGroupItemHeading>City</ListGroupItemHeading>
                        <ListGroupItemText>{address.city}</ListGroupItemText>
                    </div>

                    <div className="col-12">
                        <ListGroupItemHeading>Zipcode</ListGroupItemHeading>
                        <ListGroupItemText>{address.zipcode}
                        </ListGroupItemText>
                    </div>
                    <div className="col-12">
                        <ListGroupItemHeading>Geolocation</ListGroupItemHeading>
                        <ListGroupItemText>Lat.: {address.geo ? address.geo.lat : 'n/a'}, Lng.: {address.geo ? address.geo.lng : 'n/a'}</ListGroupItemText>
                    </div>
                </div>
            </div>
    }
}

export default Address