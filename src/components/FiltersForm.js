import React, { Component } from 'react'
import { Card, CardBody, CardHeader, Button, Form, Label, Row, Col } from 'reactstrap'
import { Field, reduxForm } from 'redux-form'

class FiltersForm extends Component {

    render = () => {
        const { handleSubmit, pristine, reset, submitting } = this.props

        return (
            <Card className="col-12 col-lg-12 col-md-12">
                <CardHeader>Filter users</CardHeader>
                <CardBody>
                    <Form onSubmit={handleSubmit}>
                        <Row className="form-group">
                            <Label htmlFor="username">Username</Label>
                            <Col md={10}><Field name="username" component="input" type="text" className="form-control" /></Col>
                        </Row>
                    </Form>
                    <Button type="button" onClick={handleSubmit} disabled={submitting}>Filter</Button>
                    <Button onClick={reset} disabled={pristine || submitting} >Clear</Button>
                </CardBody>
            </Card>
        )
    }
}

FiltersForm = reduxForm({ form: 'filters' })(FiltersForm)

export default FiltersForm