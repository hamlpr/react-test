import React, { Component } from 'react'
import { ListGroupItemHeading, ListGroupItemText } from 'reactstrap'

class Company extends Component {
    render = () => {
        const company = this.props.company

        return company &&
            !this.props.short && <div className="container">
                <div className="row">
                    <div className="col-12">
                        <ListGroupItemHeading>Name</ListGroupItemHeading>
                        <ListGroupItemText>{company.name}</ListGroupItemText>
                    </div>
                    <div className="col-12">
                        <ListGroupItemHeading>Catch phrase</ListGroupItemHeading>
                        <ListGroupItemText>{company.catchPhrase}</ListGroupItemText>
                    </div>

                    <div className="col-12">
                        <ListGroupItemHeading>Bs</ListGroupItemHeading>
                        <ListGroupItemText>{company.bs}</ListGroupItemText>
                    </div>
                </div>
            </div>
    }
}

export default Company