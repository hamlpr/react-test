import React, { Component } from 'react'
import { Card, CardBody, Button, Form, Col, Row, Label } from 'reactstrap'
import { Field, reduxForm, getFormValues } from 'redux-form'
import { connect } from 'react-redux'

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      <div>
        <input {...input} placeholder={label} type={type}/>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )

class UserForm extends Component {
    render = () => {
        const { onCancel, handleSubmit, pristine, reset, submitting } = this.props

        return (<Card >
            <CardBody>
                <Form onSubmit={handleSubmit}>
                    <Row className="form-group">
                        <Label htmlFor="name" md={2}>Name</Label>
                        <Col md={10}><Field name="name" component={renderField} type="text" className="form-control"/></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="username">Username</Label>
                        <Col md={10}><Field name="username" component={renderField} type="text" className="form-control"  required/></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="email">Email</Label>
                        <Col md={10}><Field name="email" component={renderField} type="email" className="form-control"  required/></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="phone">Phone</Label>
                        <Col md={10}><Field name="phone" component={renderField} type="tel" className="form-control" /></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="phone">Street</Label>
                        <Col md={10}><Field name="address.street" component={renderField} type="text" className="form-control" /></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="phone">City</Label>
                        <Col md={10}><Field name="address.city" component={renderField} type="text" className="form-control" /></Col>
                    </Row>
                    <Row className="form-group">
                        <Label htmlFor="phone">Suite</Label>
                        <Col md={10}><Field name="address.suite" component={renderField} type="tel" className="form-control" /></Col>
                    </Row>
                </Form>
                <Button onClick={handleSubmit} disabled={submitting}>Submit</Button>
                <Button onClick={reset} disabled={pristine || submitting} >Reset</Button>
                <Button onClick={onCancel}>Cancel</Button>
            </CardBody>
        </Card>)
    }
}

const validate = (values) => {
    const errors = {};
    const requiredFields = ['name', 'username', 'email']

    requiredFields.forEach((field) => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (values.email && !values.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
        errors.email = 'Invalid email'
    }
    return errors;
}

UserForm = reduxForm({ form: 'user', validate })(UserForm)
UserForm = connect((state) => ({ formValues: getFormValues('user')(state) }))(UserForm)
export default UserForm