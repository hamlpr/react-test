import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Navbar, NavbarBrand } from 'reactstrap';
import ListPage from './components/ListPage'
import { Provider } from 'react-redux';
import { configureStore } from './reducer/store';

const store = configureStore()

function App() {
  return (
    <Provider store={store}>
      <div>
        <Navbar dark color="primary">
          <div className="container">
            <NavbarBrand href="/">Users menagement</NavbarBrand>
          </div>
        </Navbar>
        <ListPage />
      </div>
    </Provider>
  );
}

export default App;
