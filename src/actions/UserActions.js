import {
    getUsers,
    getUser,
    deleteUser as delUser,
    putUser as editAUser,
    postUser as createUser
} from '../api/api'

export const SEARCH_USERS = 'SEARCH_USERS'

export function filterUsers(field, value) {
    return {
        type: SEARCH_USERS,
        error: false,
        payload: { field, value }
    }
}
export const FETCH_USERS = 'FETCH_USERS'

function fetchUsersStart(criteria) {
    return {
        type: FETCH_USERS,
        meta: { criteria }
    }
}

function fetchUsersComplete(criteria, users) {
    return {
        type: FETCH_USERS,
        error: false,
        payload: users,
        meta: { criteria }
    }
}

function fetchUsersError(criteria, error) {
    return {
        type: FETCH_USERS,
        payload: error,
        error: true,
        meta: { criteria }
    }
}

export function fetchUsers(criteria = {}) {
    return (dispatch) => {
        dispatch(fetchUsersStart(criteria))
        return getUsers(criteria)
            .then((users) => dispatch(fetchUsersComplete(criteria, users)))
            .catch((error) => {
                dispatch(fetchUsersError(criteria, error))
                return Promise.reject(error)
            })
    }
}

export const FETCH_USER = 'FETCH_USER'

function fetchUserStart(id) {
    return {
        type: FETCH_USER,
        meta: { id }
    }
}

function fetchUserComplete(id, user) {
    return {
        type: FETCH_USER,
        error: false,
        payload: user,
        meta: { id }
    }
}

function fetchUserError(id, error) {
    return {
        type: FETCH_USER,
        payload: error,
        error: true,
        meta: { id }
    }
}

export function fetchUser(id) {
    return (dispatch) => {
        dispatch(fetchUserStart(id))
        return getUser(id)
            .then((user) => dispatch(fetchUserComplete(id, user)))
            .catch((error) => {
                dispatch(fetchUserError(id, error))
                return Promise.reject(error)
            })
    }
}

export const CREATE_USER = 'CREATE_USER'

function createUserStart(user) {
    return {
        type: CREATE_USER,
        meta: { user }
    }
}

function createUserComplete(user) {
    return {
        type: CREATE_USER,
        error: false,
        payload: user,
        meta: {}
    }
}

function createUserError(user, error) {
    return {
        type: CREATE_USER,
        payload: error,
        error: true,
        meta: {}
    }
}
export function addUser(user) {
    return (dispatch) => {
        dispatch(createUserStart(user))
        return createUser(user)
            .then((user) => {
                return dispatch(createUserComplete(user))
            })
            .catch((error) => {
                dispatch(createUserError(user, error))
                return Promise.reject('Error')
            })
    }
}
export const EDIT_USER = 'EDIT_USER'

function editUserStart(id, user) {
    return {
        type: EDIT_USER,
        meta: { id, user, isBlocking: true }
    }
}

function editUserComplete(id, user) {
    return {
        type: EDIT_USER,
        payload: user,
        error: false,
        meta: { id }
    }
}

function editUserError(id, user, error) {
    return {
        type: EDIT_USER,
        payload: error,
        error: true,
        meta: { id, user }
    }
}
export function editUser(id, user) {
    return (dispatch) => {
        dispatch(editUserStart(id, user))
        return editAUser(id, user)
            .then(() => {
                return dispatch(editUserComplete(id, user))
            })
            .catch((error) => {
                dispatch(editUserError(id, user, error))
                return Promise.reject('Error')
            })
    }
}

export const DELETE_USER = 'DELETE_USER'



function deleteUserStart(id) {
    return {
        type: DELETE_USER,
        meta: { id, isBlocking: true }
    }
}

function deleteUserComplete(id) {
    return {
        type: DELETE_USER,
        error: false,
        meta: { id, isBlocking: true }
    }
}

function deleteUserError(id, error) {
    return {
        type: DELETE_USER,
        payload: error,
        error: true,
        meta: { id, isBlocking: true }
    }
}

export function deleteUser(id) {
    return (dispatch) => {
        dispatch(deleteUserStart(id))
        return delUser(id)
            .then(() => {
                return dispatch(deleteUserComplete(id))
            })
            .catch((error) => {
                dispatch(deleteUserError(id, error))
                return Promise.reject(error)
            })
    }
}