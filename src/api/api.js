import fetch from 'isomorphic-fetch'

const API_URL = 'https://jsonplaceholder.typicode.com/users'

export function getUser(id) {
    return fetch(API_URL + '/' + id).then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
        error => {
            var err = new Error(error.message);
            throw err;
        })
        .then(response => response.json())
    // .then(json => console.log(json))
}

export function getUsers(criteria) {

    let params = []
    Object.keys(criteria).forEach((field) => {
        if (criteria[field] !== undefined) {
            params.push(`${field}=${criteria[field]}`)
        }
    })
    const url = params.length ? API_URL + '?' + params.join('&') : API_URL
    return fetch(url).then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
        error => {
            var err = new Error(error.message);
            throw err;
        })
        .then(response => response.json())
}

export function postUser(user) {
    return fetch(API_URL, {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    }).then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
        error => {
            var err = new Error(error.message);
            throw err;
        })
        .then(response => response.json())
}

export function putUser(id, user) {
    return fetch(API_URL + '/' + id, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    }).then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
        error => {
            var err = new Error(error.message);
            throw err;
        })
        .then(response => response.json())
    // .then(json => console.log(json))
}

export function deleteUser(id) {
    return fetch(API_URL + '/' + id, {
        method: 'DELETE'
    }).then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
        error => {
            var err = new Error(error.message);
            throw err;
        })
}

export function filterUsers(criteria) {
    const filter = criteria.map((value, key) => (`${key}=${value}`)).join('&')
    const url = !filter.length ? API_URL + '?' + filter : API_URL
    return fetch(url)
        .then(response => response.json())
    // .then(json => console.log(json))
}
